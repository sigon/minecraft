import numpy as np


minecraft_ids = {}

# {minecraftname=[id, color name]}
minecraft_ids['Stone'] = [1, 'darkgray']
minecraft_ids['Grass'] = [2, 'green']
minecraft_ids['Cobblestone'] = [4, 'sgidarkgray']
minecraft_ids['Bedrock'] = [7, 'warmgrey']
minecraft_ids['WaterActive'] = [8, 'royalblue']
minecraft_ids['Water'] = [9, 'blue']
minecraft_ids['TallGrass'] = [31, 'forestgreen']
minecraft_ids['Farmland'] = [60, 'greenyellow']

# add minecraft's id
def parsing_ids(df):

    for i in range (len(df)):
        df.at[i,'MINEC_NAME'] = 'Grass' # set default to grass

        if 'building' in df.columns:
            building = df.at[i,'building']
            if building :
                df.at[i,'MINEC_NAME'] = 'Stone'

        if 'landuse' in df.columns:
            landuse = df.at[i,'landuse']
            if landuse == 'forest':
                df.at[i,'MINEC_NAME'] = 'TallGrass'
            elif landuse == 'meadow' :
                df.at[i,'MINEC_NAME'] = 'Farmland'
            elif landuse == 'orchard' :
                df.at[i,'MINEC_NAME'] = 'TallGrass'

        if 'waterway' in df.columns:
            waterway = df.at[i, 'waterway']
            if waterway == 'river':
                df.at[i,'MINEC_NAME'] = 'WaterActive'
            elif waterway == 'riverbank':
                df.at[i,'MINEC_NAME'] = 'Water'

        if 'barrier' in df.columns:
            barrier = df.at[i,'barrier']
            if barrier == 'city_wall':
                df.at[i,'MINEC_NAME'] = 'Cobblestone'
            elif barrier == 'wall' :
                df.at[i,'MINEC_NAME'] = 'Stone'
            elif barrier == 'sally_port':
                df.at[i,'MINEC_NAME'] = 'Bedrock'

    # parse feature height
    for i in range (len(df)):

        if 'height' in df.columns:
            if df.at[i,'height'] != None:
                df.at[i,'MINEC_HEIGHT'] = df.at[i,'height']

        if 'building:levels' in df.columns:
            if df.at[i,'building:levels'] != None:

                building_levels = df.at[i,'building:levels']
                building_height = int(building_levels) * 3 # convert to height
                df.at[i,'MINEC_HEIGHT'] = building_height
        else:
            if 'building' in df.columns:
                building = df.at[i,'building']
                if building :
                    df.at[i,'MINEC_HEIGHT'] = 5
            else:
                df.at[i,'MINEC_HEIGHT'] = 0
    # check for NaN values and add 0 (TODO IMPROVE THIS)
    for i in range (len(df)):
        if np.isnan(df.at[i,'MINEC_HEIGHT']):
            df.at[i,'MINEC_HEIGHT']=0
    return df
