import fiona
import rasterio
import numpy as np
from rasterio.mask import mask
from rasterio.plot import show
from rasterio.plot import show_hist
from matplotlib import pyplot
from constants import DATA_PATH
from constants import DEM_DATA_PATH

vector_file = DATA_PATH + "osm/bu_centro_3042.geojson" #'EPSG:3042'
raster_file = DEM_DATA_PATH + "el.mdt_cyl_mde_005_bu.tif" #'EPSG:3042'
out_raster = DATA_PATH + "dem/mde_005_bu_clip_3042.tif"

with fiona.open(vector_file, "r") as shapefile:
    geoms = [feature["geometry"] for feature in shapefile]

with rasterio.open(raster_file) as src:
    out_image, out_transform = mask(src, geoms, crop=True)
    # metadata for writing or exporting the data
    out_meta = src.meta.copy()
    print(src.meta)
    out_meta.update({
        'crs':'EPSG:3042'})
    extent = rasterio.plot.plotting_extent(src)

# Update the metadata to have the new shape (x and y and transform information)
out_meta.update({"driver": "GTiff",
                 "height": out_image.shape[1],
                 "width": out_image.shape[2],
                 "transform": out_transform})
print('TIF min value:' ,out_image.min())
print('TIF max value:' ,out_image.max())

# mask the nodata and plot the newly cropped raster layer
# raster_ma = np.ma.masked_equal(out_image, 0)
raster_ma = np.ma.masked_where(out_image == 0 ,
                              out_image,
                              copy=True)
# plotlib
fig, ax = pyplot.subplots(figsize = (10,10))
ax.set_title("burgos centro DEM\n clipped 5m", fontsize = 16);
#  rasterio.plot.show(source, with_bounds=True, contour=False,
# contour_label_kws=None, ax=None, title=None, transform=None,
# adjust='linear', **kwargs)

with rasterio.open(out_raster, "w", **out_meta) as dest:
    dest.write(raster_ma)
    # plot: (raster-file, band 1), mapplolit colormap, extent
    show(
        (dest, 1),
        cmap='terrain',
        extent = extent)
    print('"raster_ma" min value:' ,raster_ma.min())

    class_bins = [out_image.min(), 2, 700, 750,800, 850,900,np.inf]
    out_image_class = np.digitize(out_image, class_bins)
    print(np.unique(out_image_class))

    out_image_class_ma = np.ma.masked_where(out_image_class == 1 ,
                              out_image_class,
                              copy=True)
    show_hist(
        raster_ma,
        bins=50,
        edgecolor='white',
        lw=0.0, stacked=False, alpha=0.3,
        histtype='stepfilled', title="Histogram")
    # ax.imshow(out_image_class_ma)
    pyplot.show()
