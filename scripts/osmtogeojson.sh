#!/bin/bash
# this script use the global npm module osmtogeojson
# npm install -g osmtogeojson

MYPWD=${PWD}
PROYECTDIR="$(dirname "$MYPWD")"
OSMDATADIR=$PROYECTDIR/data/osm
OVERPASSURL="http://overpass-api.de/api/interpreter"
peral="42.0668554982705132,-4.0922578850554601,42.0826739326977588,-4.0606666633487762"
bu_centro="42.3321539989137037,-3.7298583984375004,42.3565777430638164,-3.6839389801025386"


OVERPASSQUERY="
[out:xml];
(
  node($peral);
  <;
);
out center;
"

OVERPASSQUERY_BU="
[out:xml][timeout:25][bbox:$bu_centro];
// gather results
(
  // query part for: “waterway=riverbank”
  way["barrier"="city_wall"];

    // query part for: “waterway=riverbank”
  node["waterway"="riverbank"];
  way["waterway"="riverbank"];
  relation["waterway"="riverbank"];

    // query part for: “building=cathedral”
  node["building"="cathedral"];
  way["building"="cathedral"];
  relation["building"="cathedral"];
);
out meta;
>;
out skel qt;
"

# get osm data file:
wget -O $OSMDATADIR/data.osm "$OVERPASSURL?data=$OVERPASSQUERY_BU"

# convert osm to geojson:
osmtogeojson $OSMDATADIR/data.osm > $OSMDATADIR/data.geojson
