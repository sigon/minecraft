import numpy as np
import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling
from constants import DATA_PATH

dst_crs = 'EPSG:4326'
raster_file = DATA_PATH + "dem/mde_005_bu_clip_3042.tif" #'EPSG:3042'
reprojected_raster = DATA_PATH + "dem/mde_005_bu_clip_4326.tif" #'EPSG:4326'

with rasterio.open(raster_file) as src:
    transform, width, height = calculate_default_transform(
        src.crs, dst_crs, src.width, src.height, *src.bounds)
    kwargs = src.meta.copy()
    kwargs.update({
        'crs': dst_crs,
        'transform': transform,
        'width': width,
        'height': height
    })

    with rasterio.open(reprojected_raster, 'w', **kwargs) as dst:
        for i in range(1, src.count + 1):
            reproject(
                source=rasterio.band(src, i),
                destination=rasterio.band(dst, i),
                src_transform=src.transform,
                src_crs=src.crs,
                dst_transform=transform,
                dst_crs=dst_crs,
                resampling=Resampling.nearest)
