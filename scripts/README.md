## python scrips

### conda env

```bash
# activate env
$ conda activate minecraft-dev-env
# if there changes on packages
$ conda env export > environment.yml
```

### get OSM data and convert it to geojson

osmtogeojson

https://github.com/tyrasd/osmtogeojson

```bash
$ npm install -g osmtogeojson
$ osmtogeojson file.osm > file.geojson
```

Execute bash script:

```bash
$ sh osmtogeojson.sh
```

### rasterize the geojson file (get features.tiff)

A tiff file that represents roads, paths, buildings etc according to minecraft ids.
