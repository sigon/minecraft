# script that rasterize a geojson file from osm to a tiff file
# and parse features to minecraft ids.
# conda activate rasterio-env
import rasterio
import fiona
import json
import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
from rasterio import features
from rasterio import mask
from rasterio.features import rasterize
from rasterio.plot import show
from constants import DATA_PATH
from utils.parse_minecraft import parsing_ids, minecraft_ids
from utils.color_constants import colors

vector_file = DATA_PATH + 'osm/data.geojson' #'EPSG:4326'
raster_file = DATA_PATH + 'dem/mde_005_bu_clip_4326.tif' #'EPSG:4326'
out_raster = DATA_PATH + 'feature/data_feature.tif' #'EPSG:4326'
out_raster_h = DATA_PATH + 'feature/data_feature_height.tif' #'EPSG:4326'

def show_img(img, bounds):
    left, bottom, right, top = bounds
    plt.imshow(img, cmap='gray', extent=(left, right, bottom, top))
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=90)

# open osm geojson as GeoDataFrame
df = gpd.read_file(vector_file)
print('columns list:')
print(list(df))

# add minecraft's field
df = parsing_ids(df)
# add height
# df = parsing_height(df)


# print(df[['MINEC_NAME', 'waterway']])

with rasterio.open(raster_file) as src:
    # B (RGB) blue color => colors['blue'][2]
    # minecraft COLOR =>   minecraft_ids[value][1]

    # BAND1 = R (RGB) color =>   colors[minecraft_ids[value][1]][0]
    b1_shapes = (
        (geom, colors[minecraft_ids[value][1]][0])
        for geom, value in zip(df.geometry, df.MINEC_NAME)
    )
    # BAND2 = G (RGB) color =>   colors[minecraft_ids[value][1]][1]
    b2_shapes = (
        (geom, colors[minecraft_ids[value][1]][1])
        for geom, value in zip(df.geometry, df.MINEC_NAME)
    )
    # BAND3 = B (RGB) color =>   colors[minecraft_ids[value][1]][2]
    b3_shapes = (
        (geom, colors[minecraft_ids[value][1]][2])
        for geom, value in zip(df.geometry, df.MINEC_NAME)
    )
    # BAND4 = minecraft id =>   minecraft_ids[value][0]
    b4_shapes = (
        (geom, minecraft_ids[value][0])
        for geom, value in zip(df.geometry, df.MINEC_NAME)
    )
    # BAND5 = feature height
    b5_shapes = (
        (geom, df.loc[index,'MINEC_HEIGHT'])
        for geom, index in zip(df.geometry, df.index)
    )
    print('_____________________')
    print(src.meta)

    print('_____ rasterizing.... ids_mask')
    # extract id, transform coordinates and geometry only
    ids_mask = rasterize(
        b4_shapes, # minecraft id's and geometries
        transform=src.affine,
        out_shape=src.shape,
        all_touched = True,
        fill = 2, #set defautl to grass id=2
        default_value = 2, # set default to grass id=2
        dtype=rasterio.uint16)

    print('_____ rasterizing.... R_mask')
    R_mask = rasterize(
        b1_shapes, # minecraft reds and geometries
        transform=src.affine,
        out_shape=src.shape,
        all_touched = True,
        fill = colors[minecraft_ids['Grass'][1]][0], #set default to grass
        default_value = colors[minecraft_ids['Grass'][1]][0],
        dtype=rasterio.uint16)

    print('_____ rasterizing.... G_mask')
    G_mask = rasterize(
        b2_shapes, # minecraft greens and geometries
        transform=src.affine,
        out_shape=src.shape,
        all_touched = True,
        fill = colors[minecraft_ids['Grass'][1]][1], #set default to grass
        default_value = colors[minecraft_ids['Grass'][1]][1],
        dtype=rasterio.uint16)

    print('_____ rasterizing.... B_mask')
    B_mask = rasterize(
        b3_shapes, # minecraft blues and geometries
        transform=src.affine,
        out_shape=src.shape,
        all_touched = True,
        fill = colors[minecraft_ids['Grass'][1]][2], #set default to grass
        default_value = colors[minecraft_ids['Grass'][1]][2],
        dtype=rasterio.uint16)

    print('_____ rasterizing.... height_mask')
    height_mask = rasterize(
        b5_shapes, # features height and geometries
        transform=src.affine,
        out_shape=src.shape,
        all_touched = True,
        fill = 0, # TODO
        default_value = 0, # TODO
        dtype=rasterio.uint16)

# copy raster template metadata
meta = src.meta.copy()
meta.update(nodata=999, count=1, dtype=rasterio.uint16,transform=src.affine)
print('_____updated meta________________')
print(meta)

# write bands as image (tif) by open in 'w' mode
with rasterio.open(
        out_raster, 'w', # open in write mode
        **meta) as dst:
    # dst.write_band(1, R_mask) # band R
    # dst.write_band(2, G_mask) # band G
    # dst.write_band(3, B_mask) # band B
    # dst.write_band(4, ids_mask) # minecraf ids
    # dst.write_band(5, height_mask) # feature height
    dst.write(ids_mask, indexes=1) # write band 1
with rasterio.open(
        out_raster_h, 'w', # open in write mode
        **meta) as dst:
    dst.write(height_mask, indexes=1) # write band 1


# plot mask (band 4, only minecraft ids)
plt.figure(1)
show_img(ids_mask, src.bounds)
plt.title('feature ids mask')
# plt.savefig(DATA_PATH + 'feature/data_feature.png', dpi=300)
plt.show()

# plot RGB stack
src_out = rasterio.open(out_raster)
red = src_out.read(1) # Read the grid values into numpy arrays
green = src_out.read(2)
blue = src_out.read(3)
plt.figure(2)
rgb = np.dstack((red, green, blue)) # Create RGB composite
plt.imshow(rgb)
plt.title('Data Feature RGB stack')
plt.savefig(DATA_PATH + 'feature/data_feature.png', dpi=300)
plt.show()
