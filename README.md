# MINECRAFT MAPS

minecraft id list:

https://minecraft-ids.grahamedgecombe.com/

https://github.com/tzerk/GIS-2-MC
https://knowledge.safe.com/articles/1064/how-to-make-minecraft-worlds.html

https://www.geoboxers.com/worldbloxer/samples/
https://github.com/aheadley/pynemap/blob/master/docs/color-dict_alpha.txt


steps:

* select the zone to map as vector file: ```bu_centro_4326.geojson```
* get osm data as geojson: ```sh osmtogeojson.sh```
* clip the DEM (raster) file with the zone to map: ```mask-out-raster.py```
* reproject clipped DEM to 4326: ```rasterio_reproject.py```
* rasterize vector features as minecraft ids: ```rasterize.py```
* generate Minecraft map: ```generate_map.py
